<?php 

	include("classe/conexao.php");

	if (isset($_POST['update'])) {
		
		$PK_CPF = mysqli_real_escape_string($mysqli, $_POST['PK_CPF']);
		$NOME = mysqli_real_escape_string($mysqli, $_POST['NOME']);
		$TELEFONE = mysqli_real_escape_string($mysqli, $_POST['TELEFONE']);
		$ENDERECO = mysqli_real_escape_string($mysqli, $_POST['ENDERECO']);

		if (empty($PK_CPF) || empty($NOME) || empty($TELEFONE) || empty($ENDERECO)) {
			if (empty($PK_CPF)) {
						echo "Campo CPF vazio <br/>";
					}
			if (empty($NOME)) {
						echo "Campo Nome vazio <br/>";
					}
			if (empty($TELEFONE)) {
						echo "Campo Telefone vazio <br/>";
					}
			if (empty($ENDERECO)) {
						echo "Campo Endereco vazio <br/>";
					}
			} else {
				$result = mysqli_query($mysqli, "UPDATE PESSOA SET PK_CPF='$PK_CPF', NOME='$NOME', TELEFONE='$TELEFONE', ENDERECO='$ENDERECO' WHERE PK_CPF=$PK_CPF");

				header("Location:pessoa.php");
			}

		}
?>

<?php 

	$PK_CPF = $_GET['PK_CPF'];
	$result = mysqli_query($mysqli,"SELECT * FROM PESSOA WHERE PK_CPF=$PK_CPF");

	while ($res = mysqli_Fetch_array($result)) {
		$PK_CPF=$res['PK_CPF'];
		$NOME=$res['NOME'];
		$TELEFONE=$res['TELEFONE'];
		$ENDERECO=$res['ENDERECO'];
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Editar</title>
</head>
<body>
	<form action="editar.php" method="post" name="form1">	
	<table>
		<tr>
			<td>CPF</td>
			<td><input type="text" name="PK_CPF" value="<?php echo $PK_CPF; ?>"></td>
		</tr>
		<tr>	
			<td>Nome</td>
			<td><input type="text" name="NOME" value="<?php echo $NOME; ?>"></td>
		</tr>
		<tr>
			<td>Telefone</td>
			<td><input type="text" name="TELEFONE" value="<?php echo $TELEFONE; ?>"></td>
		</tr>
		<tr></tr>	
			<td>Endereço</td>
			<td><input type="text" name="ENDERECO" value="<?php echo $ENDERECO; ?>"></td>
		</tr>
		<tr>
            <td><input type="hidden" name="PK_CPF" value=<?php echo $_GET['PK_CPF']; ?>></td>
            <td><input type="submit" name="update" value="Atualizar"></td>	
        </tr>

	</table>
</form>
</body>
</html>