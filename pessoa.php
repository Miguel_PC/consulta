<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>PESSOA</title>
		<h1>Tabela Pessoa</h1>
		<a href="index.php"><button type = "button">Menu</button></a>
		<a href="inserir.php"><button type = "button">Inserir</button></a>		
	</head>

	<body >
		<table border="1">
				<tr>
					<td>CPF</td>
					<td>Nome</td>
					<td>Telefone</td>
					<td>Endereço</td>
					<td>Ações</td>
				</tr>
				<?php 
					include("classe/conexao.php");
					$result = mysqli_query($mysqli,"SELECT * FROM PESSOA ORDER BY NOME ASC");

					while ($res = mysqli_fetch_array($result)) {
						echo "<tr>";
							echo "<td>" . $res['PK_CPF'] . "</td>";
							echo "<td>" . $res['NOME'] . "</td>";
							echo "<td>" . $res['TELEFONE'] . "</td>";
							echo "<td>" . $res['ENDERECO'] . "</td>";
								echo "<td>
								<a href=\"editar.php?PK_CPF=$res[PK_CPF]\">Editar</a> |
								<a href=\"excluir.php?PK_CPF=$res[PK_CPF]\" onClick=\"return confirm('Voce tem certeza que deseja apagar?')\">Excluir</a>
								</td>";
					}



				?>
			</table>

	</body>
</html>